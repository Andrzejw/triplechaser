# Triplechaser

Test dataset of the triple chase gas canister with three categories in the segmentation masks: top part, bottom part and not-canister class (which is everything else)

The Unity build starts to generate a dataset as soon as the program is launched, needs to be stopped after a few minues as it will generate huge amounts of data very soon. In the next iteration I'll add a GUI to select the local folder for the dataset along with a hard limit of images.  

